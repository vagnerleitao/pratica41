
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagnerg
 */
public class Pratica41 {
    private static double valorArea;
    private static double valorPerimetro;
    
    public static void main(String[] args) {
        String areac = "O valor da area do circulo e : ";
        String perc = "\nO valor do Perimetro do circulo e : ";
        String areae = "O valor da area da elipse e : ";
        String pere = "\nO valor do Perimetro da elipse e : ";
        Elipse fig=new Elipse(2.25,6.89);
        valorArea=fig.getArea();
        valorPerimetro=fig.getPerimetro();
        System.out.println(areae+valorArea+pere+valorPerimetro+"\n");
        Circulo fig1=new Circulo(2.25);
        valorArea=fig1.getArea();
        valorPerimetro=fig1.getPerimetro();
        System.out.println(areac+valorArea+perc+valorPerimetro+"\n");
    }
}
