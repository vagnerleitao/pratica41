package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagnerg
 */
public class Elipse {
    public final double eixo1;
    public final double eixo2;
    public double area;
    public double perimetro;
   
    public Elipse(double r,double s){
        eixo1=r;
        eixo2=s;
    }
    
    public double getArea(){
        area = Math.PI*eixo1*eixo2;
        return area;
    }
    
    public double getPerimetro(){
        perimetro=Math.PI*(3*(eixo1+eixo2)-Math.sqrt((3*eixo1+eixo2)*(eixo1+3*eixo2)));
        return perimetro;
    }
}
